# AnkleSocks

Tiny websockets for C++

These sockets are multi-platform and defined in **Sockets.h**, along with some small helper functions, while the majority of the sockets implementation is defined in **Socket.h**. Sockets currently use a connection based system, although anonymous data support may be added in the future.

___
# Socket Config #
The configuration for a socket is defined in the **Socket::SocketConfig** struct. This is passed to the constructor when creating a new socket. The configuration options are as follows:

### BufferSize ###
This is the default size for message buffers for the socket. Messages should not exceed this size.

### Domain ###
This is the network domain that this socket will use, essentially this lets us select IPv4 or IPv6 networking.

### Type ###
This is the networking type that the socket will use, this will also select the protocol to use, the options are as follows:
   * **TCP:** Uses reliable TCP networking
   * **UDP:** Uses unreliable UDP datagrams

### AcceptsConnections ###
This determines whether the socket can accept incoming connections. If this is **true**, then a port must be set and must be free when the socket is created.

### Port ###
If **AcceptsConnections** is set, then this is the port that the socket will bind to. Note that only one socket can bind to a given point at any time.

### MaxPendingConnections ###
If **AcceptsConnections** is set, then this determines the maximum number of pending connections a socket should handle. This normally does not need to be set.

### Callbacks: ###
The sockets also support callback based data reception. Note that the **Poll** command must still be called every frame to receive data.

#### MessageDataHandler #### 
This callback will be invoked if there is not a callback defined in **MessageTypeHandlers**. The return type from this callback will determine whether to pass the packet data to the **OnDataReceived** handlers. If the function returns **false**, then the message will be passed to **OnDataReceived** and the connection-level events, otherwise (**true**) the package will be considered to be handled and no further processing will take place. 

### MessageTypeHandler ### 
This is a dictionary of **MessageDataHandlers** that can be set to automatically handle packets with the given type code. These take precedence over the default **MessageDataHandler**, and the same rules apply. If the callback exists and it returns **true**, no further processing takes place, otherwise (**false**) it is passed down the line to **MessageDataHandler**.

### Events.OnNewConnection ###
This event is fired when a new connection is made to the socket, either from Connect, or from accepting new connections via setting the **AcceptsConnections** flag.

### Events.OnConnectionClosed ###
This event is fired when a connection to the socket has been closed.

### Events.OnSocketClosing ###
This event is fired when the socket is closing, this is fired for each connection on the socket to handle goodbye messages and other cleanup tasks.

### Events.OnDataReceived ###
This event is fired when the socket has received data on any of it's connections. Note that there is also a **OnDataReceived** on the **SocketConnection** itself if you need to listen to data events explicitly from a single connection.

### Events.OnVerifyConnection ###
This predicate is used to determine whether or not an incoming connection should be accepted. This is only used when the AcceptsConnections flag is set. If this returns **true**, then the connection is accepted, if **false**, then the connection is rejected.

___
# Socket Connections #
A connection between sockets is defined via **SocketConnection**. These connections can be made by either calling the **Connect** command on a socket, or automatically on a socket with **AcceptsConnections** set to true. These support basic read and write operations via the **MessageReader** and **MessageWriter** classes.