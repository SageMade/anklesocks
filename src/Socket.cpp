#include "Socket.h"
#include <ws2tcpip.h>
#include <string>
#include "SocketConnection.h"
#include "PacketDefs.h"
#include <chrono>
#include <stdexcept>
#include "Logger.h"

/// <summary>
/// Returns the time since this method was first called in milliseconds
/// </summary>
long long Time() {
	static auto start = std::chrono::system_clock::now();
	return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start).count();
}

 /// <summary>
/// Initializes a new instance of the <see cref="Socket"/> class.
/// </summary>
/// <param name="config">The configuration.</param>
Socket::Socket(SocketConfig config) {
	// Copy the config
	myConfig = config;

	// The total number of sockets has increased
	SocketCount++;
	// Initialize the socket subsystem
	__InitSockets();
	// We are not yet listening for connections
	isListening = false;
	// Set up the heartbeat timer
	myLastPulseTime = Time();
	// Expand the message reader to fit our buffer size
	myMessageReader.Expand(myConfig.BufferSize);
	// Gets our socket protocol
	SockProtocol proto = GetSocketProtocol(myConfig.Domain, myConfig.Type);

	// Make sure the configuration is valid
	if (IsSocketConfigValid(myConfig.Domain, myConfig.Type, proto)) {
		// Create the system socket
		mySocket = socket((int)myConfig.Domain, (int)myConfig.Type, (int)proto);
	}
	// Otherwise the socket is invalid
	else
		mySocket = BAD_SOCKET;

	// Get any errors that occured if we have a bad socket
	if (mySocket == BAD_SOCKET)
		__DebugError();
	// Otherwise we set up our socket
	else {
		// This is just true
		char opt = 1;
		// This is our timeout in seconds
		DWORD timeout = config.Timeout;
		// Sets our socket linger options (should really pull from config), see the MSDN article
		LINGER linger;
		linger.l_onoff = 0;
		linger.l_linger = 2;

		// We want to reuse the IP and keep our socket open
		setsockopt(mySocket, SOL_SOCKET, SO_REUSEADDR | SO_KEEPALIVE, &opt, sizeof(opt));
		// We also have a timeout on our socket
		setsockopt(mySocket, SOL_SOCKET, SO_RCVTIMEO, (const char*)&timeout, sizeof(timeout));
		// Set our linger options
		setsockopt(mySocket, SOL_SOCKET, SO_LINGER, (const char*)&linger, sizeof(linger));
		
		// If we accept connections, we need to bind to the port and set up listening
		if (myConfig.AcceptsConnections) {
			__Bind(myConfig.Port);
			__Listen();
		}
	}
}

Socket::Socket(SocketHandle handle) {
	mySocket = handle;
}

Socket::~Socket()
{
	// We have one less socket
	SocketCount--;
	// Close all the connections
	for (int ix = 0; ix < myConnections.size(); ix++) {
		SocketConnection* conn = myConnections[ix];
		// Invoke the closing event if it's there
		if (myConfig.Events.OnSocketClosing != nullptr)
			myConfig.Events.OnSocketClosing(this, conn);
		// Close the connection's socket
		close_socket(conn->mySocket);
		// free the connection
		delete conn;
	}
	// Close the socket
	Close();
	// Close the socket subsystem if required
	__QuitSockets();
}

void Socket::__Bind(const sockaddr* address, size_t structSize)
{
	// Simple passthough to the system socket
	int err = bind(mySocket, address, structSize);
	if (err != 0)
		Logger::Log(0) << "Failed to start listening: " << err;
}

void Socket::__Bind(USHORT port)
{
	// Create an address for the port and our IP
	int size = 0;
	sockaddr* address = CreateAddress(port, myConfig.Domain, &size);
	// Bind that mofo
	__Bind(address, size);
	// Clean up the address (no leaking memory!)
	DeleteAddress(address);
}

void Socket::__Listen() {
	// Simple passthough to the system socket
	int err = listen(mySocket, (int)myConfig.MaxPendingConnections);
	if (err != 0)
		Logger::Log(0) << "Failed to start listening: " << err;
	else
		isListening = true;
}

SocketConnection* Socket::Connect(sockaddr* address, size_t structSize)
{
	// Simple passthough to the system socket
	int resultCode = connect(mySocket, address, structSize);
	if (resultCode == -1) {
		// Debug and delete the address on failure
		__DebugError();
		DeleteAddress(address);
		return nullptr;
	}
	else {
		// We create a new socket connection for the address and add it to our connections
		SocketConnection* conn = new SocketConnection(mySocket, address, myConfig.BufferSize, myConnections.size());
		myConnections.push_back(conn);
		// Invoke callback if required
		if (myConfig.Events.OnNewConnection != nullptr)
			myConfig.Events.OnNewConnection(this, conn);
		// Return that shit!
		return conn;
	}
}

SocketConnection* Socket::Connect(const char* ip, USHORT port)
{
	// Create the address
	int size = 0;
	sockaddr* address = CreateAddress(port, myConfig.Domain, ip, &size);
	// Connect
	SocketConnection* result = Connect(address, size);
	// Clean up the address
	DeleteAddress(address);
	return result;
}

void Socket::Broadcast(const void* data, size_t size, const PacketHeader& header) {
	// Write the message
	myMessageWriter.Put(header);
	myMessageWriter.Put(data, size);
	// Broadcast the raw message
	BroadcastRaw(myMessageWriter.Buffer(), myMessageWriter.Size());
	// Flush the writer (mark message sent)
	myMessageWriter.Flush();
}

void Socket::Broadcast(const void* data, size_t size)
{
	// This is the default header
	static PacketHeader Header = { 
		TYPECODE_DATA_RAW,
		0, 0
	};
	// Compose the message
	myMessageWriter.Put(Header);
	myMessageWriter.Put(data, size);
	// Send it
	BroadcastRaw(myMessageWriter.Buffer(), myMessageWriter.Size());
	// Flush the writer
	myMessageWriter.Flush();
}

void Socket::BroadcastRaw(const void* data, size_t size)
{
	// Send to all connections
	for (int ix = 0; ix < myConnections.size(); ix++) {
		SocketConnection* conn = myConnections[ix];
		conn->SendRaw(data, size);
	}
}

MessageWriter& Socket::StartMessage()
{
	static PacketHeader Header = {
		TYPECODE_DATA_RAW,
		0, 0
	};
	// Write the default packet header
	return StartMessage(Header);
}

MessageWriter& Socket::StartMessage(const uint16_t typecode, const uint16_t channelID /*= 0*/, const uint16_t seqNum /*= 0*/)
{
	static PacketHeader Header = { 0, 0, 0 };
	// Copy that shiat
	Header.Typecode = typecode;
	Header.ChannelID = channelID;
	Header.SequenceNum = seqNum;

	// Write the default packet header
	return StartMessage(Header);
}

MessageWriter& Socket::StartMessage(const PacketHeader& header)
{
	// We don't want a start mid-message
	if (myMessageWriter.Size() > 0)
		throw new std::runtime_error("Cannot start new message, writer in use");
	// Put the header and return
	myMessageWriter.Put(header);
	return myMessageWriter;
}

void Socket::Dispatch(SocketConnection* connection) {
	// If null, send to all
	if (connection == nullptr)
		BroadcastRaw(myMessageWriter.Buffer(), myMessageWriter.Size());
	// Otherwise, send to specific connection
	else
		connection->SendRaw(myMessageWriter.Buffer(), myMessageWriter.Size());

	// Flush the writer
	myMessageWriter.Flush();
}

SocketConnection* Socket::__AcceptConnection()
{
	// We need to make an address that fits our domain
	int size = 0;
	sockaddr* address = CreateAddress(0, myConfig.Domain, &size);
	// Accept the connection, store the address
	SocketHandle result = accept(mySocket, address, &size);

	// If it was OK
	if (result != SOCKET_ERROR) {
		// Create and return the connection
		SocketConnection* conn = new SocketConnection(result, address, myConfig.BufferSize, myConnections.size());
		return conn;
	}
	else {
		// Otherwise debug and return null
		__DebugError();
		return nullptr;
	}
}

SocketConnection* Socket::GetConnection(const uint16_t index) const {
	return myConnections[index];
}

void Socket::__CullDeadSockets() {

	// Our timeout is 1ms
	static timeval timeout = { 0, 1 };
	static char buff[1];

	// Zero out the read set
	FD_ZERO(&myReadSet);
	// Mark our socket as something to watch
	FD_SET(mySocket, &myReadSet);

	SocketHandle max = mySocket;

	// Iterate over all connections
	for (int ix = 0; ix < myConnections.size(); ix++) {
		SocketHandle socket = myConnections[ix]->mySocket;
		// Mark it to be watched
		if (SOCKET_VALID(socket))
			FD_SET(socket, &myReadSet);

		// If it's ID is larger, make it the new max
		if (socket > max)
			max = socket;
	}
	// This will update our read set with any changes to our connections
	int err = select(max + 1, &myReadSet, NULL, NULL, &timeout);
	if (err == SOCKET_ERROR)
		__DebugError();

	// Iterate over all the connections
	for (int ix = 0; ix < myConnections.size(); ix++) {
		SocketConnection* conn = myConnections[ix];
		// Only update if there is a change on the socket (data in)
		if (FD_ISSET(conn->mySocket, &myReadSet)) {
			conn->myStatus = SocketStatus::Disconnected;
			// This is checking a 1 byte buffer to ensure that the socket is still alive
			int numBytes = recv(conn->mySocket, buff, 1, MSG_PEEK);
			if (numBytes == SOCKET_ERROR) {
				// If not, we close our local socket
				close_socket(conn->mySocket);
				// Invoke the callback if any
				if (myConfig.Events.OnConnectionClosed != nullptr)
					myConfig.Events.OnConnectionClosed(this, conn);
				// Delete the connection
				delete conn;
				// Erase from the vector
				myConnections.erase(myConnections.begin() + ix);
				// Make sure we don't skip an element
				ix--;
			}
		}
	}

}

void Socket::__ProcessSocketMessage(SocketConnection* conn, size_t messageSize) {

	// Start by processing out the header
	conn->myStatus = SocketStatus::Connected;
	PacketHeader header = myMessageReader.Get<PacketHeader>();

	switch (header.Typecode) {
	case TYPECODE_HEARTBEAT_OUT:

		break;
	case TYPECODE_CONN_RESPONSE:

		break;
	default:
		// Tracking if the message has been handled
		bool handled = false;

		myMessageReader.Seek(0);
		
		// First check to see if we have an explicit handler for the typecode
		if (myConfig.MessageTypeHandlers[header.Typecode] != nullptr) {
			handled = myConfig.MessageTypeHandlers[header.Typecode](this, conn, header, myMessageReader);
			myMessageReader.Seek(sizeof(PacketHeader));
		}

		// Next see if we have a generic handler and if it can handle the message
		if (!handled && myConfig.MessageDataHandler != nullptr) {
			handled = myConfig.MessageDataHandler(this, conn, header, myMessageReader);
			myMessageReader.Seek(sizeof(PacketHeader));
		}

		// Lastly, we pass through to the generic OnDataReceived message
		if (!handled) {
			conn->myReader.Reset();
			// We store a handle to it since the callback can change the read head location
			conn->myReader.LoadData(myMessageReader.myHead, myMessageReader.Remaining());
			// If we have a socket-level callback, invoke it
			if (myConfig.Events.OnDataReceived != nullptr)
				myConfig.Events.OnDataReceived(this, conn, myMessageReader);
			// If there is a connection level callback, invoke it
			if (conn->myCallback != nullptr) {
				conn->myCallback(conn, conn->myReader);
			}
		}
	}
}

void Socket::Poll()
{
	// Timeout is 1ms
	static timeval timeout = { 0, 1 };
	// Buffer is slightly larger to avoid small packet streams eating up time
	char buff[32];

	// Zero out the read set
	FD_ZERO(&myReadSet);
	// Mark our socket as something to watch
	FD_SET(mySocket, &myReadSet);
	// Default max socket is the main socket
	SocketHandle max = mySocket;
	// Iterate over all connections
	for (int ix = 0; ix < myConnections.size(); ix++) {
		SocketHandle socket = myConnections[ix]->mySocket;
		// Mark it to be watched
		if (SOCKET_VALID(socket))
			FD_SET(socket, &myReadSet);

		// If it's ID is larger, make it the new max
		if (socket > max)
			max = socket;
	}
	// This will update our read set with any changes to our connections
	int err = select(max + 1, &myReadSet, NULL, NULL, &timeout);
	if (err == SOCKET_ERROR)
		__DebugError();
	// If we have a read change on the socket itself, then we have a new connection
	if (FD_ISSET(mySocket, &myReadSet) && isListening) {
		__HandleConnection();
	}
	// Iterate over all the connections
	for (int ix = 0; ix < myConnections.size(); ix++) {
		SocketConnection* conn = myConnections[ix];
		// Only update if there is a change on the socket (data in)
		if (FD_ISSET(conn->mySocket, &myReadSet)) {
			// This is checking a small buffer to ensure that the socket is still alive
			int numBytes = recv(conn->mySocket, buff, 32, MSG_PEEK);
			if (numBytes == SOCKET_ERROR) {
				conn->myStatus = SocketStatus::Disconnected;
				// If not, we close our local socket
				close_socket(conn->mySocket);
				// Invoke the callback if any
				if (myConfig.Events.OnConnectionClosed != nullptr)
					myConfig.Events.OnConnectionClosed(this, conn);
				// Delete the connection
				delete conn;
				// Erase from the vector
				myConnections.erase(myConnections.begin() + ix);
				// Make sure we don't skip an element
				ix--;
				// We don't need the second check
				continue;
			}
			// The data received was OK
			else {			
				myMessageReader.Reset();
				size_t size = recv(conn->mySocket, myMessageReader.myBuffer, myMessageReader.myBufferSize, 0);
				myMessageReader.myDataLength = size;
				__ProcessSocketMessage(conn, size);
				myMessageReader.Reset();
			}
		}
	}

	// Remove any sockets that have died
	__CullDeadSockets();

	// This is where the heartbeat stuff happens
	long long nowTime = Time();
	if (nowTime - myLastPulseTime > 500) {
		// This is the heartbeat
		static PacketHeader Header = { TYPECODE_HEARTBEAT_OUT, 0, 0 };

		// TODO: put something useful here
		struct HeartBeat {
			uint16_t timeCode = 0;
		};
		HeartBeat currentHeartbeat;

		// Send the heartbeat to all connections
		Broadcast(&currentHeartbeat, sizeof(currentHeartbeat), Header);

		// Update the last time
		myLastPulseTime = nowTime;
	}
}

int Socket::Close()
{
	return close_socket(mySocket);
}

void Socket::__HandleConnection() {
	// Accept the incoming connection
	SocketConnection* conn = __AcceptConnection();
	// This is our response
	static PacketHeader Header = {
		TYPECODE_CONN_RESPONSE,
		0, 0
	};
	bool response = false;
	// Make sure the connection succeeded
	if (conn != nullptr) {
		// If we verify connections
		if (myConfig.Events.OnVerifyConnection != nullptr) {
			// Run our acception filter
			bool accepted = myConfig.Events.OnVerifyConnection(this, conn);
			// If they did not pass
			if (!accepted) {
				// We send a "fuck no" message to them
				response = false;
				conn->Send(&response, sizeof(response), Header);
				// And then shut them off
				close_socket(conn->mySocket);
				return;
			}
		}
		// Send the "fuck yeah" message
		response = true;
		conn->Send(&response, sizeof(response), Header);
		// Store the connections
		myConnections.push_back(conn);
		// Invoke the callbacks
		if (myConfig.Events.OnNewConnection != nullptr)
			myConfig.Events.OnNewConnection(this, conn);
	}
}

#pragma region Static Implementations

size_t Socket::SocketCount;
bool Socket::SocketSystemOnline;

SockProtocol Socket::GetSocketProtocol(SockDomain domain, SockType type)
{
	if (type == SockType::TCP)
		return SockProtocol::TCP;
	else if (type == SockType::UDP)
		return SockProtocol::UDP;
	else
		return SockProtocol::None;
}

bool Socket::IsSocketConfigValid(SockDomain domain, SockType type, SockProtocol protocol)
{
	bool isDomainValid = (domain == SockDomain::IPv4 | domain == SockDomain::IPv6);
	bool isTypeValid = (
		type == SockType::TCP | type == SockType::UDP);

	bool isProtocolValid = false;
	if (protocol == SockProtocol::TCP)
		isProtocolValid = (isDomainValid & type == SockType::TCP);
	else if (protocol == SockProtocol::UDP)
		isProtocolValid = (isDomainValid & type == SockType::UDP);

	return isDomainValid & isTypeValid & isProtocolValid;
}

// This is terrifying, ignore this

sockaddr* Socket::CreateAddress(USHORT port, SockDomain domain, int* size)
{
	sockaddr* result = nullptr;
	if (domain == SockDomain::IPv4) {
		sockaddr_in* r4 = (sockaddr_in*)malloc(sizeof(sockaddr_in));
		memset(r4, 0, sizeof(sockaddr_in));
		result = (sockaddr*)r4;
		r4->sin_family = (int)domain;
		r4->sin_addr.s_addr = INADDR_ANY;
		r4->sin_port = htons(port);
		if (size != nullptr)
			*size = (int)sizeof(sockaddr_in);
	}
	else if (domain == SockDomain::IPv6) {
		sockaddr_in6* r6 = (sockaddr_in6*)malloc(sizeof(sockaddr_in6));
		memset(r6, 0, sizeof(sockaddr_in6));
		result = (sockaddr*)r6;
		r6->sin6_family = (int)domain;
		r6->sin6_addr = in6addr_any;
		r6->sin6_port = htons(port);
		if (size != nullptr)
			*size = (int)sizeof(sockaddr_in6);
	}
	return result;
}

sockaddr* Socket::CreateAddress(USHORT port, SockDomain domain, const char* ip, int* size)
{
	sockaddr* result = nullptr;
	if (domain == SockDomain::IPv4) {
		sockaddr_in* r4 = (sockaddr_in*)malloc(sizeof(sockaddr_in));
		memset(r4, 0, sizeof(sockaddr_in));
		result = (sockaddr*)r4;
		r4->sin_family = (int)domain;
		inet_pton((int)domain, ip, &r4->sin_addr);
		r4->sin_port = htons(port);
		if (size != nullptr)
			*size = (int)sizeof(sockaddr_in);
	}
	else if (domain == SockDomain::IPv6) {
		sockaddr_in6* r6 = (sockaddr_in6*)malloc(sizeof(sockaddr_in6));
		memset(r6, 0, sizeof(sockaddr_in6));
		result = (sockaddr*)r6;
		r6->sin6_family = (int)domain;
		inet_pton((int)domain, ip, &r6->sin6_addr);
		r6->sin6_port = htons(port);
		if (size != nullptr)
			*size = (int)sizeof(sockaddr_in6);
	}
	return result;
}

// Mkay, we good again

int Socket::__InitSockets()
{
	// Only try to online it if it's down
	if (!SocketSystemOnline) {
#ifdef _WIN32
		// This is windows code!
		WSADATA wsa_data;
		int code = WSAStartup(MAKEWORD(1, 1), &wsa_data);
		SocketSystemOnline = code == 0;
		return code;
#else
		// Linux is much simpler...
		SocketSystemOnline = true;
		return 0;
#endif
	}
	// We're already up, so return A-OK!
	else
		return 0;
}

int Socket::__QuitSockets()
{
	// Only kill it if the sockets are all gone
	if (SocketCount == 0) {
		if (SocketSystemOnline) {
			SocketSystemOnline = false;
			Logger::Log(0) << "Cleaning up the socket subsystem";
#ifdef _WIN32
			return WSACleanup();
#else
			return 0;
#endif
		}
		else
			return 0;
	}
	else
		Logger::Log(0) << "Sockets are still alive, skipping socket system shutdown (" << (int)SocketCount << " Sockets alive)";
}

//Returns the last Win32 error, in string format. Returns an empty string if there is no error.
std::string GetLastErrorAsString(int error)
{
	//Get the error message, if any.
	DWORD errorMessageID = error;
	if (errorMessageID == 0)
		return std::string(); //No error message has been recorded

	LPSTR messageBuffer = nullptr;
	size_t size = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, errorMessageID, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&messageBuffer, 0, NULL);

	std::string message(messageBuffer, size);

	//Free the buffer.
	LocalFree(messageBuffer);

	return message;
}

void Socket::__DebugError()
{
	int errCode = WSAGetLastError();
	Logger::Log(0) << GetLastErrorAsString(errCode).c_str();
}

void Socket::DeleteAddress(sockaddr* address)
{
	free(address);
}

#pragma endregion

