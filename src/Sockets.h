#pragma once
#ifdef _WIN32
	/* See http://stackoverflow.com/questions/12765743/getaddrinfo-on-win32 */
	//#ifndef _WIN32_WINNT
	//#define _WIN32_WINNT 0x0501  /* Windows XP. */
	//#endif
	#include <winsock2.h>
	#include <ws2tcpip.h>
	typedef SOCKET SocketHandle;
	inline bool SOCKET_VALID(SocketHandle handle) {
		return handle != INVALID_SOCKET;
	}
	const SocketHandle BAD_SOCKET = INVALID_SOCKET;
	inline int inet_aton(const char* ip, void* addr) {
		return inet_pton(AF_INET, ip, addr);
	}
#else
	#define SOCKET_ERROR -1
	/* Assume that any non-Windows platform uses POSIX-style sockets instead. */
	#include <sys/socket.h>
	#include <arpa/inet.h>
	#include <netdb.h>  /* Needed for getaddrinfo() and freeaddrinfo() */
	#include <unistd.h> /* Needed for close() */
	typedef int SocketHandle;
	inline bool SOCKET_VALID(SocketHandle handle) {
		return handle >= 0;
	}
	const SocketHandle BAD_SOCKET = -1;
#endif

	inline int close_socket(SocketHandle socket) {
		char buff[32];
		int c = 0;
		while (c != 0 && c != SOCKET_ERROR)
			recv(socket, buff, 32, 0);

		int status = 0;

#ifdef _WIN32
		status = shutdown(socket, SD_BOTH);
		if (status == 0) { status = closesocket(socket); }
#else
		status = shutdown(socket, SHUT_RDWR);
		if (status == 0) { status = close(socket); }
#endif

		return status;
	}

	enum class SocketStatus : int {
		Unknown = 0,
		Initializing = 1,
		Connected    = 2,
		Disconnected = 3
	};

	enum class SockDomain : int {
		Unspecified = AF_UNSPEC,
		IPv4 = AF_INET,
		IPv6 = AF_INET6
	};

	enum class SockType : int {
		TCP = SOCK_STREAM,
		UDP = SOCK_DGRAM
	};

	enum class SockProtocol : int {
		None = 0,
		TCP  = IPPROTO_TCP,
		UDP  = IPPROTO_UDP
	};
		
