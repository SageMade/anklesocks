#include "SocketConnection.h"
#include "Socket.h"
#include "PacketDefs.h"

SocketConnection::~SocketConnection() {
#ifdef _WIN32
	int status = shutdown(mySocket, SD_BOTH);
	if (status == 0) { status = closesocket(mySocket); }
#else
	status = shutdown(mySocket, SHUT_RDWR);
	if (status == 0) { status = close(mySocket); }
#endif
	Socket::DeleteAddress(myAddress);
}

void SocketConnection::Send(const void* data, size_t bufferSize, const PacketHeader& header) {
	myWriter.Put(header);
	myWriter.Put(data, bufferSize);
	SendRaw(myWriter.Buffer(), myWriter.Size());
	myWriter.Flush();
}

void SocketConnection::SendRaw(const void * data, size_t bufferSize) {
	int err = send(mySocket, (const char*)data, bufferSize, 0);
	if (err == SOCKET_ERROR)
		Socket::__DebugError();
}

void SocketConnection::Send(const char* message) {
	static PacketHeader RawDataHeader = {
		TYPECODE_DATA_RAW, 0, 0
	};
	myWriter.Put(RawDataHeader);
	myWriter.Put(message);
	SendRaw(myWriter.Buffer(), myWriter.Size());
	myWriter.Flush();
}

int SocketConnection::BytesAvailable() const {
	return myReader.Remaining();
}

SocketConnection::SocketConnection(SocketHandle handle, sockaddr* address, size_t bufferSize, uint16_t clientID) {
	mySocket = handle;
	myAddress = address;
	myClientID = clientID;
	myCallback = nullptr;
	myStatus = SocketStatus::Initializing;
	myReader.Expand(bufferSize);
}
