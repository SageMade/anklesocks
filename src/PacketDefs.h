#pragma once

#include <cstdint>

#define TYPECODE_CONN_RESPONSE 1
#define TYPECODE_HEARTBEAT_OUT 2
#define TYPECODE_DATA_RAW      3

struct PacketHeader {
	uint32_t Typecode : 16;
	uint32_t ChannelID : 4;
	uint32_t SequenceNum : 12;
};