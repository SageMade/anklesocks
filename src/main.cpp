#include "Socket.h"
#include "SocketConnection.h"
#include "PacketDefs.h"
#include <iostream>
#include <chrono>
#include <future>
#include <string>
#include <mutex>

#define _CRTDBG_MAP_ALLOC  
#include <stdlib.h>  
#include <crtdbg.h>  

typedef std::unique_lock<std::mutex> LOCK;

// These are used for our threading
std::mutex  GlblMTX;
std::string GlblNewString;
bool        GlblNewMessage;

// This lets us use an async method of getting a line from the console
void GetLine() {
	std::string result = "";
	while (result != "quit") {
		std::getline(std::cin, result);
		LOCK l = LOCK(GlblMTX);
		GlblNewString = std::move(result);
		GlblNewMessage = std::move(true);
		l.unlock();
	}
}

struct NetClient {
	SocketConnection* ServerConnection;
	Socket* Socket;
	std::string Message = "";

	void Update() {
		LOCK l = LOCK(GlblMTX);
		if (GlblNewMessage) {
			Message = GlblNewString;
			GlblNewMessage = false;

			MessageWriter& writer = Socket->StartMessage();
			writer.Put(Message.c_str());
			Socket->Dispatch(ServerConnection);
		}
		l.unlock();
		Socket->Poll();
	}
};

struct NetServer {
	Socket* Socket;

	void Update() {
		Socket->Poll();

		for (int ix = 0; ix < Socket->NumConnections(); ix++) {
			SocketConnection* conn = Socket->GetConnection(ix);

			if (conn->BytesAvailable() > 0) {
				if (conn->Status() == SocketStatus::Connected) {
					MessageReader& reader = conn->GetBuffer();
					std::string text = reader.GetString();
					std::cout << "[ SERVER-IN ] 0x" << std::hex << conn->ClientID() << ": " << text << " [" << reader.DataSize() << "]" << std::endl;

					MessageWriter& writer = Socket->StartMessage();

					const char* resFormat = "%i said: %s";
					int resSize = snprintf(nullptr, 0, resFormat, conn->ClientID(), text.c_str());

					writer.Put((uint16_t)resSize);
					char* response = (char*)writer.Put(nullptr, resSize);
					sprintf_s(response, resSize + 1, resFormat, conn->ClientID(), text.c_str());

					Socket->Dispatch(nullptr);
				}
			}
		}
	}
};

struct ThisIsAVector {
	float A, B, C;
};

#define TYPECODE_VECTOR_THING 5

int main() {
	const char* quitCommand = "quit";

	{
		NetClient client;
		NetServer server;

		{
			Socket::SocketConfig config = Socket::SocketConfig();
			config.Type = SockType::TCP;
			config.Port = 6006u;
			config.AcceptsConnections = true;

			config.Events.OnNewConnection = [](Socket* socket, SocketConnection* conn) {
				std::cout << "[SERVER-INFO] A connection has been opened" << std::endl;
				conn->Send("Hello from the server!");
			};
			config.MessageTypeHandlers[TYPECODE_VECTOR_THING] = [](Socket* socket, SocketConnection* conn, const PacketHeader& header, MessageReader& reader) {
				ThisIsAVector input = reader.Get<ThisIsAVector>();
				std::cout << "[ SERVER-IN ] " << input.A << "," << input.B << "," << input.C << std::endl;
				return true;
			};

			server.Socket = new Socket(config);
			server.Socket->Poll();
		}

		{
			Socket::SocketConfig config = Socket::SocketConfig();
			config.Type = SockType::TCP;
			config.Port = 6007u;
			config.Events.OnNewConnection = [](Socket* socket, SocketConnection* conn) {
				std::cout << "[CLIENT-INFO] A connection has been opened" << std::endl;
				conn->Send("Hello world from the client!");
			};

			client.Socket = new Socket(config);

			client.ServerConnection = client.Socket->Connect("127.0.0.1", 6006u);

			client.ServerConnection->SetCallback([](SocketConnection* conn, MessageReader& reader) {
				std::cout << "[ CLIENT-IN ] " << reader.GetString() << " [" << reader.DataSize() << "]" << std::endl;
			});

			client.Socket->Poll();

			ThisIsAVector vec = { 1.0f, 2.0f, 3.0f };
			
			MessageWriter& msg = client.Socket->StartMessage(TYPECODE_VECTOR_THING);
			msg.Put(vec);
			client.Socket->Dispatch();
		}

		auto future = std::async(std::launch::async, GetLine);

		while (strcmp(client.Message.c_str(), "quit") != 0) {
			server.Update();
			client.Update();
		}

		client.ServerConnection->Send(quitCommand);

		delete client.Socket;
		delete server.Socket;
	}

	std::cout << "Press enter to quit...";
	std::cin.get();

	return 0;
}