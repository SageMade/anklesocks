#pragma once

/// <summary>
/// The base class for logger implementations. Only __PrintInternal needs to be implemented
/// </summary>
class ILogger {
public:
	virtual ~ILogger() {}
	
	/// <summary>
	/// Prints the specified message to the logger
	/// </summary>
	/// <param name="message">The message.</param>
	virtual void Print(const char* message);	
	/// <summary>
	/// Prints a formatted message (using sprintf)
	/// </summary>
	/// <param name="fmrt">The format to use</param>
	/// <param name="">The arguments to format with</param>
	virtual void PrintFmt(const char* fmrt, ...);

protected:	
	/// <summary>
	/// Handles putting a raw message on the stream
	/// </summary>
	/// <param name="message">The message to log</param>
	virtual void __PrintInternal(const char* message) = 0;
};

/// <summary>
/// This is a helper instance that is used to stream input into the logger
/// </summary>
struct LoggerLog {
	LoggerLog();
	~LoggerLog();

	char* Message;
	size_t MessageSize;

	LoggerLog& operator <<(const char* message);

	LoggerLog& operator <<(const short value);
	LoggerLog& operator <<(const unsigned short value);
	LoggerLog& operator <<(const int value);
	LoggerLog& operator <<(const unsigned int value);
	LoggerLog& operator <<(const long value);
	LoggerLog& operator <<(const unsigned long value);
	LoggerLog& operator <<(const float value);
	LoggerLog& operator <<(const double value);
	LoggerLog& operator <<(const bool value);
};

/// <summary>
/// Represents a standard logging interface
/// </summary>
class Logger {
public:	
	/// <summary>
	/// Gets the active logger instance
	/// </summary>
	static ILogger& Instance();
	
	/// <summary>
	/// Begins logging at the specified level
	/// </summary>
	/// <param name="level">The level to log at</param>
	/// <returns>A logger instance that can receive streamed inputs</returns>
	static LoggerLog Log(int level) {
		return LoggerLog();
	}
	
	/// <summary>
	/// Sets the active logger instance.
	/// </summary>
	/// <param name="instance">The instance.</param>
	static void SetInstance(ILogger* instance);

private:
	static ILogger* mySingleton;

	static ILogger* __GetDefaultLogger();
};

/// <summary>
/// A simple logger that writes to the console
/// </summary>
class StdErrLogger : public ILogger {
public:
	virtual ~StdErrLogger();

protected:
	virtual void __PrintInternal(const char* message);
};