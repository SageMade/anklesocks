#include "Logger.h"
#include <cstdarg>
#include <sstream>
#include <cstdio>
#include <iostream>
#include <cstdlib>


void ILogger::Print(const char* message) {
	__PrintInternal(message);
}

void ILogger::PrintFmt(const char* format, ...)
{
	va_list argptr;
	va_start(argptr, format);
	int size = snprintf(nullptr, 0, format, argptr);
	if (size > 0) {
		char* buffer = new char[size];
		sprintf_s(buffer, size, format, argptr);
		Print(buffer);
		delete[] buffer;
	}
	va_end(argptr);
}

ILogger* Logger::mySingleton;

ILogger* Logger::__GetDefaultLogger() {
	return new StdErrLogger();
}

ILogger& Logger::Instance() {
	if (mySingleton == nullptr)
		mySingleton = __GetDefaultLogger();
	return *mySingleton;
}

void Logger::SetInstance(ILogger* instance) {
	if (mySingleton != nullptr)
		delete mySingleton;
	mySingleton = instance;
}

StdErrLogger::~StdErrLogger() { }

void StdErrLogger::__PrintInternal(const char* message) {
	std::cerr << message << "\n";
}

LoggerLog::LoggerLog() { 
	Message = nullptr;
	MessageSize = 0;
}

LoggerLog::~LoggerLog() {
	// Todo: flush
	if (Message != nullptr) {
		Logger::Instance().Print(Message);
		free(Message);
	}
}

LoggerLog& LoggerLog::operator<<(const bool value) {
	(*this) << (value ? "true" : "false");
	return *this;
}

LoggerLog& LoggerLog::operator<<(const double value) {
	(*this) << std::to_string(value).c_str();
	return *this;
}

LoggerLog& LoggerLog::operator<<(const float value) {
	(*this) << std::to_string(value).c_str();
	return *this;
}

LoggerLog& LoggerLog::operator<<(const unsigned long value) {
	(*this) << std::to_string(value).c_str();
	return *this;
}

LoggerLog& LoggerLog::operator<<(const long value) {
	(*this) << std::to_string(value).c_str();
	return *this;
}

LoggerLog& LoggerLog::operator<<(const unsigned int value) {
	(*this) << std::to_string(value).c_str();
	return *this;
}

LoggerLog& LoggerLog::operator<<(const int value) {
	(*this) << std::to_string(value).c_str();
	return *this;
}

LoggerLog& LoggerLog::operator<<(const unsigned short value) {
	(*this) << std::to_string(value).c_str();
	return *this;
}

LoggerLog& LoggerLog::operator<<(const short value) {
	(*this) << std::to_string(value).c_str();
	return *this;
}

LoggerLog& LoggerLog::operator<<(const char* message) {
	size_t size = strlen(message);
	if (size > 0) {
		if (Message != nullptr)
			Message = (char*)realloc(Message, MessageSize + size + 1);
		else
			Message = (char*)malloc(size + 1);
		memcpy(Message + MessageSize, message, size);
		MessageSize += size;
		Message[MessageSize] = '\0';
	}
	return *this;
}
