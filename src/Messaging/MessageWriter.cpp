#include "MessageWriter.h"
#include "crc.h"

MessageWriter::MessageWriter() {
	myBufferSize = MESSAGE_BLOCK_SIZE;
	myDataLength = 0;
	// We use c-style alloc, fight me
	myBuffer = (char*)malloc(myBufferSize);
	myHead = myBuffer;
}

MessageWriter::~MessageWriter() {
	free(myBuffer);
}

void* MessageWriter::Put(const void* data, size_t size) {
	// If the buffer isn't large enough, expandong it
	if (myDataLength + size > myBufferSize)
		__ResizeBuffer(myBufferSize + MESSAGE_BLOCK_SIZE);
	// Copy data if it is not nullptr
	if (data != nullptr)
		memcpy(myHead, data, size);
	// Grab the head location to return later
	void* result = myHead;
	// Offset the message length
	myDataLength += size;
	myHead = myBuffer + myDataLength;
	// Return that MOFO
	return result;
}

void MessageWriter::Flush() {
	myDataLength = 0;
	myHead = myBuffer;
}

void MessageWriter::__ResizeBuffer(size_t newSize) {
	// Allocate a new buffer and zero it
	char* newBuff = (char*)malloc(newSize);
	memset(newBuff, 0, newSize);
	// Copy the old buffer
	memcpy(newBuff, myBuffer, myBufferSize);
	// Free the old buffer
	free(myBuffer);
	// Update the ptr
	myBuffer = newBuff;
	// Update size and re-calculate head
	myBufferSize = newSize;
	myHead = myBuffer + myDataLength;
}

void MessageWriter::Put(const std::string& value) {
	// Get the size of the string
	uint16_t size = value.size();
	// Make sure the buffer is big enough
	if (myDataLength + size + sizeof(uint16_t) > myBufferSize)
		__ResizeBuffer(myBufferSize + MESSAGE_BLOCK_SIZE);
	// Write the size to the stream
	*(uint16_t*)myHead = size;
	// Advance head
	myHead += sizeof(uint16_t);
	// Copy the string to the stream
	memcpy(myHead, value.c_str(), size);
	myDataLength += size + sizeof(uint16_t);
	// Update the head
	myHead = myBuffer + myDataLength;
}