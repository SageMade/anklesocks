#include "MessageReader.h"
#include "crc.h"
#include "PacketDefs.h"

MessageReader::MessageReader(size_t bufferSize) {
	myBufferSize = bufferSize;
	myOffset = 0;
	myDataLength = 0;
	myBuffer = (char*)malloc(myBufferSize);
	myHead = myBuffer;
}

MessageReader::~MessageReader() {
	free(myBuffer);
}

void MessageReader::LoadData(const char* data, size_t size) {
	// If the buffer isn't large enough, expandong it
	if (myDataLength + size > myBufferSize)
		__ResizeBuffer(myDataLength + ((size / MESSAGE_BLOCK_SIZE) + 1) * MESSAGE_BLOCK_SIZE);
	// Copy the data over and expand the size
	memcpy(myBuffer + myDataLength, data, size);
	myDataLength += size;
}

void* MessageReader::Get(size_t size) {
	// Check for empty message
	if (myDataLength == 0)
		throw std::out_of_range("The message is empty");
	// If the read would be beyond the end of the bounds
	if (myOffset + size > myDataLength)
		throw std::out_of_range("Attempting to read beyond the end of message");
	// Store the pointer
	void* result = myBuffer;
	// Offset the head
	myOffset += size;
	myHead = myBuffer + myDataLength;
	// Return
	return result;
}

void MessageReader::SeekFromHeader(size_t offset) {
	Seek(offset + sizeof(PacketHeader));
}

void MessageReader::Expand(size_t bufferSize) {
	if (myBufferSize < bufferSize)
		__ResizeBuffer(bufferSize);
}

void MessageReader::Reset() {
	myDataLength = 0;
	myOffset = 0;
	myHead = myBuffer;
}

void MessageReader::__ResizeBuffer(size_t newSize) {
	// Allocate a new buffer and zero it
	char* newBuff = (char*)malloc(newSize);
	memset(newBuff, 0, newSize);
	// Copy the old buffer
	memcpy(newBuff, myBuffer, myBufferSize);
	// Free the old buffer
	free(myBuffer);
	// Update the ptr
	myBuffer = newBuff;
	// Update size and re-calculate head
	myBufferSize = newSize;
	myHead = myBuffer + myDataLength;
}

std::string MessageReader::GetString() {
	// Make sure not past the end of message
	if (myDataLength == 0)
		throw std::out_of_range("The message is empty");
	// Read the size and advance the head
	uint16_t size = *(uint16_t*)myHead;
	myHead += sizeof(uint16_t);
	// Create the string (will copy from the buffer)
	std::string result = std::string(myHead, size);
	// Offset the head
	myOffset += size + sizeof(uint16_t);
	myHead = myBuffer + myOffset;
	// Return
	return result;
}
