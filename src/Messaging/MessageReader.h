#pragma once
#include <cstdint>
#include <cstring>
#include <memory>
#include <cstdint>
#include <stdexcept>
#include <string>

#define MESSAGE_BLOCK_SIZE 4096

class Socket;

/// <summary>
/// A utility class that wraps a buffer, used to read complex data structures from binary streams
/// </summary>
class MessageReader {
public:
	/// <summary>
	/// Creates a new message reader with a given buffer size
	/// </summary>
	/// <param name="bufferSize">The size of the internal buffer to use for messages</param>
	MessageReader(size_t bufferSize = MESSAGE_BLOCK_SIZE);
	/// <summary>
	/// Destroys a message reader and cleans up it's underlying data
	/// </summary>
	~MessageReader();

	/// <summary>
	/// Expands this reader's internal buffer to fit the given size
	/// </summary>
	/// <param name="bufferSize">The size of the buffer</param>
	void Expand(size_t bufferSize);

	/// <summary>
	/// Loads a message into this reader
	/// </summary>
	/// <param name="data">The message data, this will be copied from</param>
	/// <param name="size">The size of the message</param>
	void LoadData(const char* data, size_t size);

	/// <summary>
	/// Gets a structure or primitive from the message
	/// </summary>
	/// <returns>A copy of the item as read from the message</returns>
	template <typename T, typename = std::enable_if<!std::is_pointer<T>::value>>
	T Get() {
		// Make sure the message isn't empty
		if (myDataLength == 0)
			throw std::out_of_range("The message is empty");
		static_assert(!std::is_pointer<T>::value, "Cannot deserialize a pointer.");
		// Make sure not past the end of message
		if (myOffset + sizeof(T) > myDataLength)
			throw std::out_of_range("Attempting to read beyond the end of message");

		// Get a ref to result to avoid copy
		T& result = *(T*)myHead;
		// Advance the head
		myOffset += sizeof(T);
		myHead = myBuffer + myOffset;
		// Return result (copy happens here)
		return result;
	}
	/// <summary>
	/// Reads and returns a string from the stream
	/// </summary>
	/// <returns>A string as read from the stream</returns>
	std::string GetString();
	/// <summary>
	/// Returns a limited lifetime raw pointer from the message.This exists until Reset is called on the reader.
	///  If being stored, be sure to copy the result
	/// </summary>
	/// <param name="size">The size to read from the buffer</param>
	/// <returns>A temporary pointer to the data</returns>
	void* Get(size_t size);

	/// <returns>Gets the remaining number of bytes left in the message</returns>
	size_t Remaining() const { return myDataLength - myOffset; }

	/// <returns>A pointer to the reader's underlying buffer</returns>
	void* Buffer() const { return myBuffer; }
	/// <returns>The reader's offset from the beginning of the message</returns>
	size_t Offset() const { return myOffset; }
	/// <returns>The size of the reader's internal buffer</returns>
	size_t BufferSize() const { return myBufferSize; }
	/// <returns>The size of the message currently loaded in the reader</returns>
	size_t DataSize() const { return myDataLength; }

	/// <summary>
	/// Moves the offset to the given position in the data stream. Note that packet headers
	/// start at 0 in a message
	/// </summary>
	/// <param name="pos">The absolute position in the message</param>
	void Seek(size_t pos) { myOffset = pos > myDataLength ? myDataLength : pos; }
	/// <summary>
	/// Moves the offset to the given position after the packet header in the message
	/// </summary>
	/// <param name="pos">The position in the data block of the message</param>
	void SeekFromHeader(size_t offset);

protected:
	friend class Socket;

	char* myBuffer;
	char* myHead;
	size_t myBufferSize;
	size_t myDataLength;
	size_t myOffset;

	/// <summary>
	/// Handles resizing the internal buffer
	/// </summary>
	/// <param name="newSize">The new size for the buffer</param>
	void __ResizeBuffer(size_t newSize);

	void Reset();
};
