#pragma once
#include <cstring>
#include <memory>
#include <cstdint>
#include <string>

#define MESSAGE_BLOCK_SIZE 4096

/// <summary>
/// A utility class that wraps a buffer, used to write complex data structures to binary streams
/// </summary>
class MessageWriter {
public:
	/// <summary>
	/// Creates a new message writer
	/// </summary>
	MessageWriter();
	/// <summary>
	/// Destroys a message writer
	/// </summary>
	~MessageWriter();

	/// <summary>
	/// Pushes a structure into the buffer
	/// </summary>
	/// <param name="value">The value to push into the message</param>
	template <typename T, typename = std::enable_if<!std::is_pointer<T>::value>>
	void Put(const T& value) {
		static_assert(!std::is_pointer<T>::value, "Cannot serialize a pointer, please dereference first.");
		if (myDataLength + sizeof(T) > myBufferSize)
			__ResizeBuffer(myBufferSize + MESSAGE_BLOCK_SIZE);
		memcpy(myHead, &value, sizeof(T));
		myDataLength += sizeof(T);
		myHead = myBuffer + myDataLength;
	}
	/// <summary>
	/// Pushes a string into the buffer. Note that strings in messages are formatted as
	/// uint16_t size;
	/// char     data[size];
	/// </summary>
	/// <param name="value">The string to push into the message</param>
	void Put(const char* value) { Put((std::string)value); }
	/// <summary>
	/// Pushes a string into the buffer. Note that strings in messages are formatted as
	/// uint16_t size;
	/// char     data[size];
	/// </summary>
	/// <param name="string">The string to push into the message</param>
	void Put(const std::string& string);
	/// <summary>
	/// Pushes the given data into the buffer, if data is nullptr, data is not copied.
	/// This can be used to reserve space in a message(ex: for string formatting)
	/// </summary>
	/// <param name="data">The data to write to the message, or nullptr to reserve space</param>
	/// <param name="size">The size of the data to write or reserve</param>
	/// <returns>The address in memory that the buffer is written to</returns>
	void* Put(const void* data, size_t size);

	/// <returns>The underlying buffer for this message writer</returns>
	void* Buffer() const { return myBuffer; }
	/// <returns>The size of the message being written</returns>
	size_t Size() const { return myDataLength; }

	/// <summary>
	/// Resets the message's write head and data length to default
	/// </summary>
	void Flush();

protected:
	char* myBuffer;
	char* myHead;
	size_t myBufferSize;
	size_t myDataLength;

	/// <summary>
	/// Handles resizing the internal buffer
	/// </summary>
	/// <param name="newSize">The new size for the buffer</param>
	void __ResizeBuffer(size_t newSize);
};

