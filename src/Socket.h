#pragma once
#include "Sockets.h"
#include "Messaging/MessageWriter.h"
#include "Messaging/MessageReader.h"
#include <vector>
#include <map>
#include <cstdint>

#ifndef DEFAULT_CONNECTION_BUFFER_SIZE
#define DEFAULT_CONNECTION_BUFFER_SIZE 4096
#endif
#ifndef DEFAULT_SOCKET_TYPE
#define DEFAULT_SOCKET_TYPE SockType::UDP
#endif
#ifndef DEFAULT_SOCKET_DOMAIN
#define DEFAULT_SOCKET_DOMAIN SockDomain::IPv4
#endif

class SocketConnection;
class Socket;
struct PacketHeader;

/// <summary>
/// Represents a delegate that handles when the status of a socket connection has changed
/// </summary>
typedef void(*ConnectionChangedDelegate)(Socket* parent, SocketConnection* connection);
/// <summary>
/// Represents a delegate that handles when a socket has received data
/// </summary>
typedef void(*SocketDataReceivedCallback)(Socket* socket, SocketConnection* connection, MessageReader& reader);
/// <summary>
/// Represents a predicate function that validates a connection before accepting it
/// </summary>
typedef bool(*VerifyConnectionCallback)(Socket* socket, SocketConnection* connection);
/// <summary>
/// Represents a delegate that handles a raw socket message
/// </summary>
typedef bool(*SocketMessageHandler)(Socket* socket, SocketConnection* connection, const PacketHeader& header, MessageReader& reader);

/// <summary>
/// 
/// </summary>
class Socket {
public:
	/// <summary>
	/// Represents the configuration of a socket
	/// </summary>
	struct SocketConfig {
		/// <summary>
		/// The default buffer sizes to use for message reading and writing
		/// </summary>
		size_t               BufferSize;
		/// <summary>
		/// The domain that the socket operates in (IPv6 or IPv6)
		/// </summary>
		SockDomain           Domain;
		/// <summary>
		/// The type of the socket (TCP or UDP)
		/// </summary>
		SockType             Type;
		/// <summary>
		/// Whether or not to bind to a port and accept incoming connection requests
		/// </summary>
		bool                 AcceptsConnections;
		/// <summary>
		/// The port to bind to if AcceptsConnections is true
		/// </summary>
		USHORT               Port;
		/// <summary>
		/// The maximum number of connections to proccess at a time if AcceptsConnections is set
		/// </summary>
		USHORT               MaxPendingConnections;
		/// <summary>
		/// The default handler for unknown/raw packet types
		/// </summary>
		SocketMessageHandler MessageDataHandler;
		/// <summary>
		/// A lookup dictionary for message handlers, keyed on packet typecode
		/// </summary>
		std::map<uint16_t, SocketMessageHandler> MessageTypeHandlers;		
		/// <summary>
		/// The timeout for the socket in milliseconds (default 2000)
		/// </summary>
		unsigned long Timeout;

		struct EventBindings {
			/// <summary>
			/// The callback to fire when a new socket connection is made
			/// </summary>
			ConnectionChangedDelegate  OnNewConnection;
			/// <summary>
			/// The callback to fire when a new socket connection is closed
			/// </summary>
			ConnectionChangedDelegate  OnConnectionClosed;
			/// <summary>
			/// The callback to fire on each connection when the socket is closing
			/// </summary>
			ConnectionChangedDelegate  OnSocketClosing;
			/// <summary>
			/// The callback to fire when a message is received at the socket level
			/// </summary>
			SocketDataReceivedCallback OnDataReceived;
			/// <summary>
			/// The predicate to use to verify incomming connection requests
			/// </summary>
			VerifyConnectionCallback   OnVerifyConnection;

			EventBindings() :
				OnNewConnection(nullptr),
				OnConnectionClosed(nullptr),
				OnSocketClosing(nullptr),
				OnDataReceived(nullptr),
				OnVerifyConnection(nullptr) { }

		} Events;

		SocketConfig() :
			Port(0u),
			AcceptsConnections(false),
			MessageDataHandler(nullptr),
			MaxPendingConnections(6u),
			BufferSize(DEFAULT_CONNECTION_BUFFER_SIZE),
			Domain(DEFAULT_SOCKET_DOMAIN),
			Type(DEFAULT_SOCKET_TYPE),
			Events(EventBindings()),
			Timeout(2000),
			MessageTypeHandlers(std::map<uint16_t, SocketMessageHandler>()){}
	};

	/// <summary>
	/// Creates a new socket from the given configuration
	/// </summary>
	/// <param name="config">The configuration to use</param>
	Socket(SocketConfig config);
	virtual ~Socket();

	// Delete the copy and assigment operators
	Socket(const Socket& other) = delete;
	Socket& operator =(const Socket& other) = delete;
	
	/// <summary>
	/// Gets the socket's configuration
	/// </summary>
	const SocketConfig& GetConfig() const { return myConfig; }
	/// <summary>
	/// Gets the number of connections active on this socket
	/// </summary>
	int NumConnections() const { return myConnections.size(); }
	/// <summary>
	/// Gets a connection from the socket based on index
	/// </summary>
	/// <param name="index">The index of the connection to get</param>
	SocketConnection* GetConnection(const uint16_t index) const;

	/// <summary>
	/// Sets the configuration
	/// </summary>
	/// <param name="callback">The callback to bind</param>
	void SetNewConnectionCallback(ConnectionChangedDelegate callback) { myConfig.Events.OnNewConnection = callback; }
	/// <summary>
	/// Gets the new connection callback.
	/// </summary>
	/// <returns>config.Events.OnNewConnection</returns>
	ConnectionChangedDelegate GetNewConnectionCallback() const { return myConfig.Events.OnNewConnection; }
	
	/// <summary>
	/// Sets the connection closed callback.
	/// </summary>
	/// <param name="callback">The callback.</param>
	void SetConnectionClosedCallback(ConnectionChangedDelegate callback) { myConfig.Events.OnConnectionClosed = callback; }
	/// <summary>
	/// Gets the connection closed callback.
	/// </summary>
	/// <returns>config.Events.OnConnectionClosed</returns>
	ConnectionChangedDelegate GetConnectionClosedCallback() const { return myConfig.Events.OnConnectionClosed; }

	/// <summary>
	/// Sets the data callback.
	/// </summary>
	/// <param name="callback">The callback.</param>
	void SetDataCallback(SocketDataReceivedCallback callback) { myConfig.Events.OnDataReceived = callback; }
	/// <summary>
	/// Gets the data callback.
	/// </summary>
	/// <returns>config.Events.OnDataReceived</returns>
	SocketDataReceivedCallback GetDataCallback() const { return myConfig.Events.OnDataReceived; }

	/// <summary>
	/// This callback is called for each connection when a socket is closing down
	/// </summary>
	void SetSocketClosingCallback(ConnectionChangedDelegate callback) { myConfig.Events.OnSocketClosing = callback; }
	/// <summary>
	/// Gets the socket closing callback.
	/// </summary>
	/// <returns>config.Events.OnSocketClosing</returns>
	ConnectionChangedDelegate GetSocketClosingCallback() const { return myConfig.Events.OnSocketClosing; }
	
	/// <summary>
	/// Connects the specified address.
	/// </summary>
	/// <param name="address">The address to bind to, do not use after calling this method (may be freed)</param>
	/// <param name="structSize">Size of the structure.</param>
	/// <returns>The resulting connection</returns>
	SocketConnection* Connect(sockaddr* address, size_t structSize);
	/// <summary>
	/// Connects the specified ip and port.
	/// </summary>
	/// <param name="ip">The ip.</param>
	/// <param name="port">The port.</param>
	/// <returns>The resulting connection</returns>
	SocketConnection* Connect(const char* ip, USHORT port);
	
	/// <summary>
	/// Broadcasts the specified data to all connected sockets
	/// </summary>
	/// <param name="data">The data to send</param>
	/// <param name="size">The size of the message</param>
	/// <param name="header">The packet header to use</param>
	void Broadcast(const void* data, size_t size, const PacketHeader& header);
	/// <summary>
	/// Broadcasts the specified data to all connected sockets, using a RAW_DATA packet header
	/// </summary>
	/// <param name="data">The data to send</param>
	/// <param name="size">The size of the message</param>
	void Broadcast(const void* data, size_t size);
	/// <summary>
	/// Broadcasts the specified raw data to all connected sockets, without any packet headers
	/// </summary>
	/// <param name="data">The data to send</param>
	/// <param name="size">The size of the message</param>
	void BroadcastRaw(const void* data, size_t size);
	
	/// <summary>
	/// Resets the socket's internal message writer an prepares it for writing, using the RAW_DATA
	/// packet type. Will fail if data is waiting to be written
	/// </summary>
	/// <returns>The socket's message writer</returns>
	MessageWriter& StartMessage();	
	/// <summary>
	/// Resets the socket's internal message writer an prepares it for writing
	/// Will fail if data is waiting to be written
	/// </summary>
	/// <param name="header">The header to write to the message</param>
	/// <returns>The socket's message writer</returns>
	MessageWriter& StartMessage(const PacketHeader& header);
	MessageWriter& StartMessage(const uint16_t typecode, const uint16_t channelID = 0, const uint16_t seqNum = 0);
	/// <summary>
	/// Dispatches the current message to the specified connection. Of connection is nullptr, will
	/// broadcast to all connected sockets
	/// </summary>
	/// <param name="connection">The connection.</param>
	void Dispatch(SocketConnection* connection = nullptr);
		
	/// <summary>
	/// Polls the underlying socket, reading messages and dispatching events. Should be called in a loop
	/// </summary>
	void Poll();	
	/// <summary>
	/// Closes the underlying socket
	/// </summary>
	/// <returns>The return code for socket closure</returns>
	int Close();

	
	/// <summary>
	/// Creates a new socket address for the given port and domain, this will point to any IP address
	/// </summary>
	/// <param name="port">The port of the address</param>
	/// <param name="domain">The domain of the address (IPv4 vs IPv6)</param>
	/// <param name="size">A pointer to an integer that will receive the size of the resulting structure in bytes</param>
	/// <returns>A new socket address</returns>
	static sockaddr* CreateAddress(USHORT port, SockDomain domain, int* size);
	/// <summary>
	/// Creates a new socket address for the given port, ip and domain, this will point to a specific IP address
	/// </summary>
	/// <param name="port">The port of the address</param>
	/// <param name="domain">The domain of the address (IPv4 vs IPv6)</param>
	/// <param name="size">A pointer to an integer that will receive the size of the resulting structure in bytes</param>
	/// <param name="ip">The dot notation representation of the IP to bind to</param>
	/// <returns>A new socket address</returns>
	static sockaddr* CreateAddress(USHORT port, SockDomain domain, const char* ip, int* size);	
	/// <summary>
	/// Deletes an address that was created via the CreateAddress commands, used since the structures get weird (C style)
	/// </summary>
	/// <param name="address">The address to delete</param>
	static void DeleteAddress(sockaddr* address);
	
	/// <summary>
	/// Gets the socket protocol for a given socket domain and type
	/// </summary>
	/// <param name="domain">The domain of the socket</param>
	/// <param name="type">The type of socket</param>
	/// <returns>The protocol to use</returns>
	static SockProtocol GetSocketProtocol(SockDomain domain, SockType type);	
	/// <summary>
	/// Determines whether socket configuration is valid.
	/// </summary>
	/// <param name="domain">The domain of the socket</param>
	/// <param name="type">The type of the socket</param>
	/// <param name="protocol">The protocol of the socket</param>
	/// <returns>
	///   <c>true</c> if the configuration is valid, otherwise, <c>false</c>.
	/// </returns>
	static bool IsSocketConfigValid(SockDomain domain, SockType type, SockProtocol protocol);

private:
	friend class SocketConnection;
	Socket(SocketHandle handle);

	std::vector<SocketConnection*> myConnections;
	fd_set        myReadSet;
	bool          isOpen;
	bool          isListening;
	SocketHandle  mySocket;
	long long     myLastPulseTime;
	MessageWriter myMessageWriter;
	MessageReader myMessageReader;

	SocketConfig myConfig;

	static size_t SocketCount;
	static bool   SocketSystemOnline;
	
	/// <summary>
	/// Handles accepting a new connection
	/// </summary>
	void __HandleConnection();
	
	/// <summary>
	/// Binds the socket to the specified address
	/// </summary>
	/// <param name="address">The address to bind to</param>
	/// <param name="structSize">Size of the address structure</param>
	void __Bind(const sockaddr* address, size_t structSize);
	/// <summary>
	/// Binds the socket to the specified port, on any IP
	/// </summary>
	/// <param name="port">The port to bind to</param>
	void __Bind(USHORT port);	
	/// <summary>
	/// Configures the sockets to start listening for incoming connections
	/// </summary>
	void __Listen();	
	/// <summary>
	/// Removes the dead socket connections.
	/// </summary>
	void __CullDeadSockets();	
	/// <summary>
	/// Processes a message from a socket connection
	/// </summary>
	/// <param name="conn">The connection.</param>
	/// <param name="messageSize">Size of the message.</param>
	void __ProcessSocketMessage(SocketConnection* conn, size_t messageSize);	
	/// <summary>
	/// Handles the socket-level accepting of a new socket connection
	/// </summary>
	/// <returns>The new socket connection</returns>
	SocketConnection* __AcceptConnection();
	
	/// <summary>
	/// Initializes the socket subsystem.
	/// </summary>
	/// <returns>The error code from initialization</returns>
	static int __InitSockets();	
	/// <summary>
	/// Shuts down the socket subsystem.
	/// </summary>
	/// <returns>The error code from shutdown</returns>
	static int __QuitSockets();	
	/// <summary>
	/// Debugs the error.
	/// </summary>
	static void __DebugError();

};