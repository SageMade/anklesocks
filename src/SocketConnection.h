#pragma once
#include "Sockets.h"
#include "Messaging/MessageWriter.h"
#include "Messaging/MessageReader.h"
#include <cstdint>

class Socket;
class SocketConnection;
struct PacketHeader;

typedef void(*DataReceivedCallback)(SocketConnection* connection, MessageReader& reader);

class SocketConnection {
public:
	~SocketConnection();

	void Send(const void* data, size_t bufferSize, const PacketHeader& header);
	void SendRaw(const void* data, size_t bufferSize);
	void Send(const char* message);
	int  BytesAvailable() const;
	MessageReader& GetBuffer() { myReader.SeekFromHeader(0); return myReader; }

	uint16_t ClientID() const { return myClientID; }

	void SetCallback(DataReceivedCallback value) { myCallback = value; }
	DataReceivedCallback GetCallback() const { return myCallback; }

	SocketStatus Status() const { return myStatus; }

protected:
	friend class Socket;

	SocketConnection(SocketHandle handle, sockaddr* address, size_t bufferSize, uint16_t clientID);

	//void Flush() { myBufferSize = 0; }

	uint16_t     myClientID;
	SocketStatus myStatus;
	SocketHandle mySocket;
	sockaddr*    myAddress;
	DataReceivedCallback myCallback;
	MessageWriter myWriter;
	MessageReader myReader;
};